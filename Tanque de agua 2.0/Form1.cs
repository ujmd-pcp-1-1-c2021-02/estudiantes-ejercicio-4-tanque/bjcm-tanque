﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tanque_de_agua
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_llenar_Click(object sender, EventArgs e)
        {
            tmr_llenar.Enabled = true;
            tmr_vaciar.Enabled = false;

        }

        private void btn_vaciar_Click(object sender, EventArgs e)
        {
            tmr_llenar.Enabled = false;
            tmr_vaciar.Enabled = true;
        }

        private void btn_parar_Click(object sender, EventArgs e)
        {
            tmr_llenar.Enabled = false;
            tmr_vaciar.Enabled = false;
        }

        private void tmr_llenar_Tick(object sender, EventArgs e)
        {
            if (pnlnivel.Height < pnlatras.Height)
            {
                pnlnivel.Height += 2;
            }
            else
            {
                tmr_llenar.Enabled = false;
            }
        }

        private void tmr_vaciar_Tick(object sender, EventArgs e)
        {
            if (pnlnivel.Height > 0)
            {
                pnlnivel.Height -= 2;
            }
            else
            {
                tmr_vaciar.Enabled = false;
            }
        }

        private void tmr_variables_Tick(object sender, EventArgs e)
        {
            if (tmr_llenar.Enabled == true)
            {
                pbx_llenar.BackColor = Color.LightGreen;
            }
            else
            {
                pbx_llenar.BackColor = Color.FromArgb(0,32,0);
            }

            if (tmr_vaciar.Enabled == true)
            {
                pbx_vaciar.BackColor = Color.LightGreen;
            }
            else
            {
                pbx_vaciar.BackColor = Color.FromArgb(0, 32, 0);
            }

            if (tmr_llenar.Enabled == false & tmr_vaciar.Enabled == false)
            {
                pbx_parar.BackColor = Color.Red;
            }
            else
            {
                pbx_parar.BackColor = Color.FromArgb(32, 0, 0);
            }
        }
    }
}
