﻿namespace Tanque_de_agua
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tanque_img = new System.Windows.Forms.PictureBox();
            this.pnlatras = new System.Windows.Forms.Panel();
            this.pnlnivel = new System.Windows.Forms.Panel();
            this.btn_llenar = new System.Windows.Forms.Button();
            this.btn_vaciar = new System.Windows.Forms.Button();
            this.btn_parar = new System.Windows.Forms.Button();
            this.tmr_llenar = new System.Windows.Forms.Timer(this.components);
            this.tmr_vaciar = new System.Windows.Forms.Timer(this.components);
            this.tmr_variables = new System.Windows.Forms.Timer(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.pbx_parar = new System.Windows.Forms.PictureBox();
            this.pbx_llenar = new System.Windows.Forms.PictureBox();
            this.pbx_vaciar = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.tanque_img)).BeginInit();
            this.pnlatras.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_parar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_llenar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_vaciar)).BeginInit();
            this.SuspendLayout();
            // 
            // tanque_img
            // 
            this.tanque_img.Image = global::Tanque_de_agua.Properties.Resources.Recurso_1tank2;
            this.tanque_img.Location = new System.Drawing.Point(407, 219);
            this.tanque_img.Name = "tanque_img";
            this.tanque_img.Size = new System.Drawing.Size(316, 268);
            this.tanque_img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.tanque_img.TabIndex = 0;
            this.tanque_img.TabStop = false;
            // 
            // pnlatras
            // 
            this.pnlatras.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pnlatras.Controls.Add(this.pnlnivel);
            this.pnlatras.Location = new System.Drawing.Point(424, 289);
            this.pnlatras.Name = "pnlatras";
            this.pnlatras.Padding = new System.Windows.Forms.Padding(4);
            this.pnlatras.Size = new System.Drawing.Size(23, 188);
            this.pnlatras.TabIndex = 1;
            // 
            // pnlnivel
            // 
            this.pnlnivel.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.pnlnivel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlnivel.Location = new System.Drawing.Point(4, 174);
            this.pnlnivel.Name = "pnlnivel";
            this.pnlnivel.Size = new System.Drawing.Size(15, 10);
            this.pnlnivel.TabIndex = 2;
            // 
            // btn_llenar
            // 
            this.btn_llenar.Location = new System.Drawing.Point(12, 22);
            this.btn_llenar.Name = "btn_llenar";
            this.btn_llenar.Size = new System.Drawing.Size(75, 23);
            this.btn_llenar.TabIndex = 3;
            this.btn_llenar.Text = "Llenar";
            this.btn_llenar.UseVisualStyleBackColor = true;
            this.btn_llenar.Click += new System.EventHandler(this.btn_llenar_Click);
            // 
            // btn_vaciar
            // 
            this.btn_vaciar.Location = new System.Drawing.Point(12, 85);
            this.btn_vaciar.Name = "btn_vaciar";
            this.btn_vaciar.Size = new System.Drawing.Size(75, 23);
            this.btn_vaciar.TabIndex = 4;
            this.btn_vaciar.Text = "Vaciar";
            this.btn_vaciar.UseVisualStyleBackColor = true;
            this.btn_vaciar.Click += new System.EventHandler(this.btn_vaciar_Click);
            // 
            // btn_parar
            // 
            this.btn_parar.Location = new System.Drawing.Point(12, 144);
            this.btn_parar.Name = "btn_parar";
            this.btn_parar.Size = new System.Drawing.Size(75, 23);
            this.btn_parar.TabIndex = 5;
            this.btn_parar.Text = "Parar";
            this.btn_parar.UseVisualStyleBackColor = true;
            this.btn_parar.Click += new System.EventHandler(this.btn_parar_Click);
            // 
            // tmr_llenar
            // 
            this.tmr_llenar.Tick += new System.EventHandler(this.tmr_llenar_Tick);
            // 
            // tmr_vaciar
            // 
            this.tmr_vaciar.Tick += new System.EventHandler(this.tmr_vaciar_Tick);
            // 
            // tmr_variables
            // 
            this.tmr_variables.Enabled = true;
            this.tmr_variables.Interval = 50;
            this.tmr_variables.Tick += new System.EventHandler(this.tmr_variables_Tick);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.panel3.Controls.Add(this.pbx_parar);
            this.panel3.Controls.Add(this.pbx_llenar);
            this.panel3.Controls.Add(this.pbx_vaciar);
            this.panel3.Controls.Add(this.btn_parar);
            this.panel3.Controls.Add(this.btn_llenar);
            this.panel3.Controls.Add(this.btn_vaciar);
            this.panel3.Location = new System.Drawing.Point(12, 152);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(156, 187);
            this.panel3.TabIndex = 6;
            // 
            // pbx_parar
            // 
            this.pbx_parar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pbx_parar.Location = new System.Drawing.Point(102, 144);
            this.pbx_parar.Name = "pbx_parar";
            this.pbx_parar.Size = new System.Drawing.Size(24, 22);
            this.pbx_parar.TabIndex = 8;
            this.pbx_parar.TabStop = false;
            // 
            // pbx_llenar
            // 
            this.pbx_llenar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.pbx_llenar.Location = new System.Drawing.Point(102, 22);
            this.pbx_llenar.Name = "pbx_llenar";
            this.pbx_llenar.Size = new System.Drawing.Size(24, 22);
            this.pbx_llenar.TabIndex = 7;
            this.pbx_llenar.TabStop = false;
            // 
            // pbx_vaciar
            // 
            this.pbx_vaciar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.pbx_vaciar.Location = new System.Drawing.Point(102, 86);
            this.pbx_vaciar.Name = "pbx_vaciar";
            this.pbx_vaciar.Size = new System.Drawing.Size(24, 22);
            this.pbx_vaciar.TabIndex = 6;
            this.pbx_vaciar.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 612);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pnlatras);
            this.Controls.Add(this.tanque_img);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.tanque_img)).EndInit();
            this.pnlatras.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_parar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_llenar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_vaciar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox tanque_img;
        private System.Windows.Forms.Panel pnlatras;
        private System.Windows.Forms.Panel pnlnivel;
        private System.Windows.Forms.Button btn_llenar;
        private System.Windows.Forms.Button btn_vaciar;
        private System.Windows.Forms.Button btn_parar;
        private System.Windows.Forms.Timer tmr_llenar;
        private System.Windows.Forms.Timer tmr_vaciar;
        private System.Windows.Forms.Timer tmr_variables;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pbx_parar;
        private System.Windows.Forms.PictureBox pbx_llenar;
        private System.Windows.Forms.PictureBox pbx_vaciar;
    }
}

